const inventory=require('./inventory');

function prob6(inventory){
	let BMWAndAudi=[]
	for(let i=0;i<inventory.length;i++){
		if(inventory[i].car_make=='BMW' || inventory[i].car_make=='Audi'){
			BMWAndAudi.push(inventory[i])
		}
	}

	var BMWAndAudiJSON = JSON.stringify(BMWAndAudi);

	return BMWAndAudiJSON;
}

module.exports=prob6;