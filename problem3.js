const inventory=require('./inventory');

function prob3(inventory){
	let objects=inventory;
	objects.sort(function(a,b){
		var t1=a.car_model.toUpperCase();
		var t2=b.car_model.toUpperCase();
		return (t1<t2) ?-1 : (t1>t2)? 1:0;

	});
	return objects;
}

module.exports=prob3;